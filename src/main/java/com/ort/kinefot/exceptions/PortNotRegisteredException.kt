package com.ort.kinefot.exceptions

class PortNotRegisteredException : RuntimeException("El puerto especificado no se encuentra dentro de los posibles.")
