package com.ort.kinefot.service

import com.ort.kinefot.dto.HeartbeatDTO
import com.ort.kinefot.dto.MonitorResponseDTO
import com.ort.kinefot.entities.NodeInfo

interface MonitorService {

    fun registerHeartbeat(heartbeat: HeartbeatDTO): MonitorResponseDTO

    fun getActiveNodes(): List<NodeInfo>

}
