package com.ort.kinefot.service


import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import com.ort.kinefot.dto.ErrorDTO
import com.ort.kinefot.dto.HeartbeatDTO
import com.ort.kinefot.dto.MonitorResponseDTO
import com.ort.kinefot.entities.NodeInfo
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.concurrent.ThreadLocalRandom
import kotlin.collections.ArrayList


@Service
class MonitorServiceImpl : MonitorService {

    //EL PRIMER NODO QUE SE REGISTRA ES EL LIDER, SI ESE NODO CAE, HAY CAMBIO DE LIDER, MIENTRAS TANTO NO CAMBIA.

    private var nodesOfClusterData = mutableMapOf<String, NodeInfo>()

    private var lastHeartbeat = Pair<String,Calendar>("",Calendar.getInstance())

    private var leaderNode:String = ""

    override fun registerHeartbeat(heartbeat: HeartbeatDTO): MonitorResponseDTO {
        setLastHeartbeat(heartbeat.nodeId)
        if(clusterIsNotRegistered(heartbeat))registerCluster(heartbeat)
        else nodesOfClusterData[heartbeat.nodeId]= NodeInfo(heartbeat.nodeId, (nodesOfClusterData[heartbeat.nodeId]!!.timeOfStart), Calendar.getInstance(), iAmLeader(heartbeat.nodeId))
        return MonitorResponseDTO(iAmLeader(heartbeat.nodeId))
    }

    fun registerCluster(heartbeat: HeartbeatDTO){
        if(leaderNode.equals(""))
            setLeader(heartbeat.nodeId)
        nodesOfClusterData[heartbeat.nodeId]= NodeInfo(heartbeat.nodeId, Calendar.getInstance(), Calendar.getInstance(), iAmLeader(heartbeat.nodeId))
    }

    fun setLastHeartbeat(node: String){
        lastHeartbeat = Pair<String,Calendar> (node,Calendar.getInstance())
    }

    fun clusterIsNotRegistered(heartbeat: HeartbeatDTO):Boolean {
        return !nodesOfClusterData.containsKey(heartbeat.nodeId)
    }

    fun iAmLeader(node : String):Boolean{
        return leaderNode.equals(node)
    }

    fun setLeader(node : String){
        leaderNode = node
    }


    override fun getActiveNodes(): List<NodeInfo>{
        var activeNodes:MutableList<NodeInfo> = ArrayList<NodeInfo>()
        nodesOfClusterData.map { it.value}
                .forEach {
                    activeNodes.add(it)
                 }
        return activeNodes
    }

    //Funcion que elige nodo aleatorio si se llega a necesitar para eleccion de lider como scheduled task
    fun chooseNewLeader ():String{
        val leaderNodePosition = ThreadLocalRandom.current().nextInt(0, 3) //Random number from 0,1,2
        val leaderKey = nodesOfClusterData.keys.elementAt(leaderNodePosition)
        leaderNode = leaderKey
        return leaderKey
    }

    fun getLeaderInfo(): NodeInfo?{
        if(!leaderNode.equals("")){
            var leader = nodesOfClusterData[leaderNode]
            leader!!.isLeader = true
            return leader
        }else{
            return NodeInfo() //Devolvemos un nodo vacio
        }
    }

    @Scheduled(fixedRate = 60000/4)// 1/4 de minuto (15 segundos)
    fun checkIfNodesAreDown(){
        print("Corre tarea que chequea si algún nodo esta bajo.--------------------------------------------\n")

        nodesOfClusterData.values
                .filter { it.expired() }
                .map { it.node }
                .forEach {
                    setLeader("");
            nodesOfClusterData.remove(it)
            if(nodesOfClusterData.keys.size > 0)
               setLeader(nodesOfClusterData.keys.first())
        }
    }

    private fun NodeInfo.expired(): Boolean =
        ChronoUnit.SECONDS.between(lastHeartbeat.toInstant(),Calendar.getInstance().toInstant()) > 5


}
