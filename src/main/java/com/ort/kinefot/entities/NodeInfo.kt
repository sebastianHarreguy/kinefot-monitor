package com.ort.kinefot.entities

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

data class NodeInfo(val node:String,@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") val timeOfStart:Calendar, @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") val lastHeartbeat:Calendar, var isLeader:Boolean){
    constructor() : this("",Calendar.getInstance(),Calendar.getInstance(),false)


}
