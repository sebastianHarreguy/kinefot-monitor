package com.ort.kinefot.dto

import com.ort.kinefot.entities.NodeInfo

data class ErrorDTO(val mensaje: String?)

data class MonitorResponseDTO(val isLeader: Boolean)

data class HeartbeatDTO(val nodeId: String)

data class ActiveNodesDTO(val activeNodes: List<NodeInfo>)
