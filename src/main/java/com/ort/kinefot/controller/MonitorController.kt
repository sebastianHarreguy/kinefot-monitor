package com.ort.kinefot.controller

import com.ort.kinefot.dto.HeartbeatDTO
import com.ort.kinefot.dto.MonitorResponseDTO
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import com.ort.kinefot.dto.*
import com.ort.kinefot.entities.NodeInfo
import com.ort.kinefot.service.MonitorService

@RestController
@RequestMapping("monitor")
class MonitorController
@Autowired constructor(val service: MonitorService) {

    companion object {

        private val LOGGER = LoggerFactory.getLogger(MonitorController::class.java)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    fun registerHearbeat(@RequestBody port: HeartbeatDTO): MonitorResponseDTO {
        return service.registerHeartbeat(port)
    }

    @GetMapping("/active_nodes")
    @ResponseStatus(HttpStatus.OK)
    fun getActiveNodes(): List<NodeInfo>{
        return service.getActiveNodes()
    }
}
